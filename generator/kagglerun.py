import sys
sys.path.insert(0, "../input/libs")
from poet import Poet
from torch import Tensor
import torch
import random
import json
import os
import numpy as np



force_cpu = False

if torch.cuda.is_available() and force_cpu is not True:
    device = 'cuda'
    use_cuda = True
    print("Running on GPU")
else:
    device = 'cpu'
    use_cuda = False
    print("Running on CPU")
    print("Note: on Google Colab, make sure to select:")
    print("      Runtime / Change Runtime Type / Hardware accelerator: GPU")

model_finbot_params = {
    "neurons": 256,
    "layers": 2,

}
model_params = model_finbot_params

print("model Must exist!!!")
checkpoint = torch.load("../input/model0.pt")

i2c = checkpoint['i2c']
c2i = checkpoint['c2i']
vocab_size = len(i2c)
poet = Poet(vocab_size, model_params['neurons'],
            model_params['layers'], vocab_size, device).to(device)
poet.load_state_dict(checkpoint['state_dict'])


def one_hot(p, dim):
    o = np.zeros(p.shape+(dim,), dtype=int)
    for y in range(p.shape[0]):
        for x in range(p.shape[1]):
            o[y, x, p[y, x]] = 1
    return o


def generate(model, n, start=None, i2c=None, c2i=None):
    s = ''
    torch.set_grad_enabled(False)
    if start == None or len(start) == 0:
        start = ' '
    model.init_hidden(1)
    for c in start:
        X = np.array([[c2i[c]]])
        Xo = one_hot(X, model.output_size)
        Xt = Tensor(torch.from_numpy(
            np.array(Xo, dtype=np.float32))).to(model.device)
        ypl = model.forward(Xt, 1)
        ypl2 = ypl.view(-1, model.output_size)
        yp = model.softmax(ypl2)
    for i in range(n):
        ypc = Tensor.cpu(yp.detach())  # .cpu()
        y_pred = ypc.numpy()
        inds = list(range(model.output_size))
        ind = np.random.choice(inds, p=y_pred.ravel())
        s = s+i2c[ind]
        X = np.array([[ind]])
        Xo = one_hot(X, model.output_size)
        Xt = Tensor(torch.from_numpy(
            np.array(Xo, dtype=np.float32))).to(model.device)
        ypl = model.forward(Xt, 1)
        ypl2 = ypl.view(-1, model.output_size)
        yp = model.softmax(ypl2)
    torch.set_grad_enabled(True)
    return s


tgen1500 = generate(poet, 1500, "\n\n", i2c, c2i)
with open('txtoutfortrain1500.txt', 'w') as file:
    file.write(tgen1500)

tgen2000 = generate(poet, 2000, "\n\n", i2c, c2i)
with open('txtoutfortrain2000.txt', 'w') as file:
    file.write(tgen2000)


def doDialog(model, prompt):
    # temperature = 0.6  # 0.1 (frozen character) - 1.3 (creative/chaotic character)
    endPrompt = '.'  # the endPrompt character is the end-mark in answers.
    # look for number of maxEndPrompts until answer is finished.
    maxEndPrompts = 4
    maxAnswerSize = 2048  # Maximum length of the answer
    minAnswerSize = 64  # Minimum length of the answer

    # print("Please enter some dialog.")
    # print("The net will answer according to your input.")
    # print("'bye' for end,")
    # print("'reset' to reset the conversation context,")
    # # print("'temperature=<float>' [0.1(frozen)-1.0(creative)]")
    # print("    to change character of the dialog.")
    # print("    Current temperature={}.".format(temperature))
    # print()
    # xso = None
    # bye = False

    # while not bye:
    #     print("> ", end="")
    #     prompt = input()
    #     if prompt == 'bye':
    #         bye = True
    #         print("Good bye!")
    #         continue
    answer = generate(poet, 1000, prompt, i2c, c2i)
    #     # print(xso.replace("\\n","\n"))
    #     textlib.source_highlight(tgen, 10)
    return answer

# answer = doDialog(poet,'kerro sun etunimi')
# print('kerro sun etunimi-->',answer)

# answer = doDialog(poet,'voitaisko tavata joskus')
# print('voitaisko tavata joskus-->',answer)

# answer = doDialog(poet,'kauniit tissit')
# print('kauniit tissit-->',answer)
