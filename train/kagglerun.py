import sys
sys.path.insert(0, "../input/libs")
from textlibrary import TextLibrary
from poet import Poet
from torch import Tensor
import torch.nn as nn
import torch
import random
import json
import os
import numpy as np
import time
import math


libdesc = {
    "name": "TinyFinbot",
    "description": "twiiter dialogs",
    "lib": '../input/finbot.txt'

}

force_cpu = False

if torch.cuda.is_available() and force_cpu is not True:
    device = 'cuda'
    use_cuda = True
    print("Running on GPU")
else:
    device = 'cpu'
    use_cuda = False
    print("Running on CPU")


textlib = TextLibrary(libdesc["lib"])

model_finbot_params = {
    "model_name": "lib",
    "vocab_size": len(textlib.i2c),
    "neurons": 256,
    "layers": 2,
    "learning_rate": 5.e-4, #was 1.e-3
    "steps": 80,
    "batch_size": 1024 #was 128 256 512
}

model_params = model_finbot_params
batch_size = model_params['batch_size']
vocab_size = model_params['vocab_size']
steps = model_params['steps']

def timeSince(since):
    now = time.time()
    s = now - since
    m = math.floor(s / 60)
    s -= m * 60
    return '%dm %ds' % (m, s)

def one_hot(p, dim):
    o = np.zeros(p.shape+(dim,), dtype=int)
    for y in range(p.shape[0]):
        for x in range(p.shape[1]):
            o[y, x, p[y, x]] = 1
    return o


def get_data():
    X, y = textlib.get_random_sample_batch(batch_size, steps)
    Xo = one_hot(X, vocab_size)

    # Xt = Tensor(torch.from_numpy(np.array(Xo,dtype=np.float32)), requires_grad=False, dtype=torch.float32, device=device)
    # yt = Tensor(torch.from_numpy(y), requires_grad=False, dtype=torch.int32, device=device)
    Xt = Tensor(torch.from_numpy(np.array(Xo, dtype=np.float32))).to(device)
    Xt.requires_grad_(False)
    yt = torch.LongTensor(torch.from_numpy(
        np.array(y, dtype=np.int64))).to(device)
    yt.requires_grad_(False)
    return Xt, yt


def show_gpu_mem(context="all"):
    if use_cuda:
        print("[{}] Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(context, torch.cuda.memory_allocated(
        ), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))


"""## Create a poet"""
exists = os.path.isfile('../input/model0.pt')
if exists:
    # Store configuration file values
    print("model exist!!!")
    checkpoint = torch.load("../input/model0.pt")
    poet = Poet(vocab_size, model_params['neurons'],
                model_params['layers'], vocab_size, device).to(device)
    poet.load_state_dict(checkpoint['state_dict'])
    opti = torch.optim.Adam(
        poet.parameters(), lr=model_params['learning_rate'])
    opti.load_state_dict(checkpoint['optimizer'])

else:
    print("fresh start")
    poet = Poet(vocab_size, model_params['neurons'],
                model_params['layers'], vocab_size, device).to(device)
    opti = torch.optim.Adam(
        poet.parameters(), lr=model_params['learning_rate'])

"""## Training helpers"""

criterion = nn.CrossEntropyLoss()

bok = 0


def train(Xt, yt, bPr=False):
    poet.zero_grad()

    poet.init_hidden(Xt.size(0))
    output = poet(Xt, steps)

    olin = output.view(-1, vocab_size)
    _, ytp = torch.max(olin, 1)
    ytlin = yt.view(-1)

    pr = 0.0
    if bPr:  # Calculate precision
        ok = 0
        nok = 0
        for i in range(ytlin.size()[0]):
            i1 = ytlin[i].item()
            i2 = ytp[i].item()
            if i1 == i2:
                ok = ok + 1
            else:
                nok = nok+1
            pr = ok/(ok+nok)

    loss = criterion(olin, ytlin)
    ls = loss.item()
    loss.backward()
    opti.step()

    return ls, pr


"""## The actual training"""

start = time.time()
ls = 0
nrls = 0

intv = 500

for e in range(180000):  # 600000 OK 5.7 hours; 300000 OK 5.2hours
    Xt, yt = get_data()
    if (e+1) % intv == 0:
        l, pr = train(Xt, yt, True)
    else:
        l, pr = train(Xt, yt, False)
    ls = ls+l
    nrls = nrls+1
    if (e+1) % intv == 0:
        print("Time: {} Loss: {} Precision: {}".format(timeSince(start),ls/nrls, pr))
        # if use_cuda:
        #     print("Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(torch.cuda.memory_allocated(
        #     ), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))
        nrls = 0
        ls = 0

print("Memory allocated: {} max_alloc: {} cached: {} max_cached: {}".format(torch.cuda.memory_allocated(
), torch.cuda.max_memory_allocated(), torch.cuda.memory_cached(), torch.cuda.max_memory_cached()))

# def detectPlagiarism(generatedtext, textlibrary, minQuoteLength=10):
#     textlibrary.source_highlight(generatedtext, minQuoteLength)

# detectPlagiarism(tgen, textlib)


def save_checkpoint(state, filename='model0.pt'):
    torch.save(state, filename)
    # if is_best:
    #     shutil.copyfile(filename, 'model_best.pth')


# best_prec1 = 64.4

save_checkpoint({
    'epoch': e,
    'arch': "poet8",
            'state_dict': poet.state_dict(),
            # 'best_prec1': best_prec1,
            'optimizer': opti.state_dict(),
            'i2c': textlib.i2c,
            'c2i': textlib.c2i,
})
