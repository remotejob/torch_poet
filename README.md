# torch_poet
https://colab.research.google.com/github/domschl/torch-poet/blob/master/torch_poet.ipynb

kaggle datasets create -p data/
kaggle datasets version -p data/ -m "Updated data fin"


kaggle kernels push -p train/
kaggle kernels status sipvip/torchpoet
kaggle kernels output sipvip/torchpoet -p output/
cp output/model0.pt data/model0.pt
cd data
tar cfv libs.tar libs/
cd -
kaggle datasets version -r tar -p data/ -m "Updated data poet4"
kaggle datasets status sipvip/torchpoetdata


kaggle kernels push -p generator/
kaggle kernels status sipvip/torchpoetgen
kaggle kernels output sipvip/torchpoetgen -p output/